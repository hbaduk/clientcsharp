﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        Socket baglanti;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            connectBaglanti();
        }

        public void connectBaglanti()
        {
            TextBox objTextBox = (TextBox)textBox1;
            string ipaddress = objTextBox.Text;
            int intportno = Int32.Parse(textBox2.Text);
            baglanti = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            baglanti.Connect(IPAddress.Parse(ipaddress), intportno);
        }

        public void button2_Click(object sender, EventArgs e)
        {
            sendMessage();
        }

        public void sendMessage()
        {
            TextBox objTextBox4 = (TextBox)textBox4;
            string message = objTextBox4.Text;
            byte[] byData = System.Text.Encoding.ASCII.GetBytes(message);
            baglanti.Send(byData);
        }
    }
}
